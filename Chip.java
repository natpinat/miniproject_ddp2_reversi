/**
 * A Class for chips crutial for Player and Board
 */
public class Chip {
    private Hexagonal hexSlot;
    private Player player;

    /**
     * Creating chip in Player class If the hexagon slot is empty then the chip can
     * be placed
     * 
     * @param hexSlot a hexagon slot
     */
    public Chip(Hexagonal hexSlot, Player player) {
        if (hexSlot.getIsFilled() == false) {
            this.player = player;
            this.hexSlot = hexSlot;
            this.hexSlot.setIsFilled(true);
            this.hexSlot.setChip(this);
        }
    }

    // set player
    public void setPlayer(Player player) {
        this.player = player;
    }

    // do every get below here
    public Hexagonal getHexSlot() {
        return this.hexSlot;
    }

    public Player getPlayer() {
        return this.player;
    }
}