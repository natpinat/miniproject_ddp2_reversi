/**
 * A Class for Hexagon with coordinate of x, y and z
 */
public class Hexagonal {
    private int x;
    private int y;
    private int z;
    private Chip chip;
    private boolean isFilled;

    /**
     * Creating a hexagonal slot
     * 
     * @param x A x coordinate
     * @param y A y coordinate
     * @param z A z coordinate
     */
    public Hexagonal(int x, int y, int z) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.isFilled = false;
    }

    // changing the isFilled variable
    public void setIsFilled(boolean t) {
        this.isFilled = t;
    }

    public void setChip(Chip e) {
        this.chip = e;
    }

    // do every get below here
    public int getX() {
        return this.x;
    }

    public int getY() {
        return this.y;
    }

    public int getZ() {
        return this.z;
    }

    public Chip getChip() {
        return this.chip;
    }

    public boolean getIsFilled() {
        return this.isFilled;
    }

    @Override
    public String toString() {
        return String.format("Hexagonal %d %d %d %b", this.x, this.y, this.z, this.isFilled);
    }
}