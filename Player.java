import java.util.ArrayList;

/**
 * A Crutial class containing array of chips for each players
 */
public class Player {
    private ArrayList<Chip> chips = new ArrayList<>();
    private int totalChips;
    private String color;
    private String name;

    // Create new Player with name
    public Player(String name) {
        this.name = name;
    }

    /**
     * Placing a chip in an empty slot and adding it to array of chips
     * 
     * @param hex A hexagonal slot where to place chip
     */
    public void addChipToArrayHex(Hexagonal hex) {
        chips.add(new Chip(hex, this));
    }

    /**
     * Adding a chip to array, method especially for converting other chips
     * 
     * @param chip A chip that need to be added to array of chips
     */
    public void addChipToArrayCon(Chip chip) {
        this.chips.add(chip);
        chip.setPlayer(this);
    }

    // do every set below here
    public void setName(String name) {
        this.name = name;
    }

    public void setColor(String color) {
        this.color = color;
    }

    // do every get below here
    public int getTotalChips() {
        totalChips = chips.size();
        return totalChips;
    }

    public String getColor() {
        return this.color;
    }

    public String getName() {
        return this.name;
    }

    public ArrayList<Chip> getChips() {
        return this.chips;
    }

    @Override
    public String toString() {
        return String.format("%s -- total chips : %d", this.name, this.getTotalChips());
    }

}