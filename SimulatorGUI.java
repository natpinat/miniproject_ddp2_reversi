import javafx.application.*;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

public class SimulatorGUI extends Application{
    @Override
    public void start(Stage stage){
        GridPane gp = new GridPane();
        gp.setAlignment(Pos.CENTER);
        gp.setVgap(10);
        gp.setHgap(10);
        gp.setPadding(new Insets(20, 20, 20, 20));
        HBox reversi = new HBox();
        reversi.setAlignment(Pos.CENTER);
        reversi.setSpacing(10);
        Text reversitxt = new Text("Hexagon Reversion");
        reversitxt.setFont(Font.font("arial",FontWeight.BOLD,20));
        reversi.getChildren().add(reversitxt);
        gp.add(reversi,3,3,3,1);

        ToggleGroup group = new ToggleGroup();
        gp.add(new Label("How many Player?"),3,4,3,1);
        RadioButton rad = new RadioButton();
        rad.setText("2");
        rad.setToggleGroup(group);
        RadioButton rad1 = new RadioButton();
        rad1.setText("3");
        rad1.setToggleGroup(group);
        RadioButton rad2 = new RadioButton();
        rad2.setText("4");
        rad2.setToggleGroup(group);
        gp.add(rad, 3, 5,1,1);
        gp.add(rad1, 4, 5,1,1);
        gp.add(rad2, 5, 5,1,1);

        ToggleGroup group2 = new ToggleGroup();
        gp.add(new Label("Board Times (input only a number)"),3,7,3,1);
        RadioButton radt = new RadioButton();
        radt.setText("2");
        radt.setToggleGroup(group2);
        RadioButton radt1 = new RadioButton();
        radt1.setText("4");
        radt1.setToggleGroup(group2);
        RadioButton radt2 = new RadioButton();
        radt2.setText("6");
        radt2.setToggleGroup(group2);
        gp.add(radt, 3, 8,1,1);
        gp.add(radt1, 4, 8,1,1);
        gp.add(radt2, 5, 8,1,1);
        Button but = new Button("Play");
        gp.add(but,5,10,1,1);

        GridPane rev = new GridPane();



        but.setOnAction(event ->{
            Scene play = new Scene(rev, 600, 600);
            if (group.getSelectedToggle()==rad){
                if (group2.getSelectedToggle()==radt){
                    Board boardy = new Board(2,2);
                }
                else if(group2.getSelectedToggle()==radt1){
                    Board boardy = new Board(2,4);
                }
                else {
                    Board boardy = new Board(2,6);
                }
            }
            else if (group.getSelectedToggle()==rad){
                if (group2.getSelectedToggle()==radt){
                    Board boardy = new Board(3,2);
                }
                else if(group2.getSelectedToggle()==radt1){
                    Board boardy = new Board(3,4);
                }
                else {
                    Board boardy = new Board(3,6);
                }
            }
            else {
                if (group2.getSelectedToggle()==radt){
                    Board boardy = new Board(2,2);
                }
                else if(group2.getSelectedToggle()==radt1){
                    Board boardy = new Board(2,4);
                }
                else {
                    Board boardy = new Board(2,6);
                }
                
            }
            
            stage.setScene(play);
            stage.setResizable(true);
            stage.show();
        });

        Scene startNewGame = new Scene(gp, 600, 600);
        stage.setTitle("Hexagon Reversion");
        stage.setScene(startNewGame);
        stage.setResizable(true);
        stage.show();
    }

    public static void main(String[]args){
        launch();
    }

}