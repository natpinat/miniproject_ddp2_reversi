import java.util.Scanner;

/**
 * A simple class for test runs
 */
public class Runny {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        // input how many player
        System.out.print("Player? (please don't play alone) ");
        int b = input.nextInt();
        while (b <= 1) {
            System.out.println("input 2 or more");
            b = input.nextInt();
        }

        // input how big the board (only a number)
        System.out.print("Boardtimes(place a number)? ");
        int j = input.nextInt();
        while (j <= 0) {
            System.out.println("at least input 1 or more");
            j = input.nextInt();
        }

        // Creating board
        Board boardy = new Board(j, b);

        // Checking if the board is build and which index is there
        for (Hexagonal x : boardy.getHexArray()) {
            System.out.println(x.toString());
        }
        System.out.println("There's " + boardy.getHexArray().size() + " slots");

        // looping for playing
        int i = 1;
        while (boardy.checkAllIsFilled() == false) {
            // each player has a turn
            for (Player d : boardy.getPlayers()) {
                int x;
                int y;
                int z;
                // if the board is all filled then break
                if (boardy.checkAllIsFilled() == true) {
                    break;
                } else {
                    System.out.println("======================================");
                    System.out.println(i + " : " + d.toString());
                    System.out.println("Make sure to enter only number with format ex. 0 0 0");
                    x = input.nextInt();
                    y = input.nextInt();
                    z = input.nextInt();

                    // if the index is not listed then loop till correct
                    while (boardy.callHexSlot(x, y, z) == null || boardy.callHexSlot(x, y, z).getIsFilled() == true
                            || boardy.checkAllIsFilled() == true) {
                        System.out.println("**************************");
                        System.out.println("try again [index invalid]");
                        System.out.println("**************************");
                        x = input.nextInt();
                        y = input.nextInt();
                        z = input.nextInt();
                    }
                    d.addChipToArrayHex(boardy.callHexSlot(x, y, z));
                    boardy.chipsFlipper(d);
                    System.out.println("======================================");
                    System.out.println("Current : " + d.toString());
                    i++;
                }
            }
            if (boardy.checkAllIsFilled() == true) {
                break;
            }
        }

        // the looping process has done
        System.out.println("======================================");
        System.out.println("Let's count score!");

        // for print out the rank along with total chips most to least
        for (Player dx : boardy.checkWinner()) {
            System.out.println(dx.toString());
        }
        System.out.println("======================================");
        input.close();
    }
}