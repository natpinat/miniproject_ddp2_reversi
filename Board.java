import java.util.ArrayList;

/**
 * A Class that constructs the board mainly filled with methods that occurs on
 * the board
 */
public class Board {
    private ArrayList<Hexagonal> hexArray = new ArrayList<>();
    private ArrayList<Player> players = new ArrayList<>();
    private ArrayList<Hexagonal> hextemp = new ArrayList<>();
    private ArrayList<Chip> chipTemps = new ArrayList<>();

    /**
     * Constructor for setting initial board to play
     * 
     * @param manyHexagonals How many hexagonals to use
     * @param manyPlayers    How many players are playing
     */
    public Board(int manyHexagonals, int manyPlayers) {
        // set initial board done for real
        for (int currentNum = 0; currentNum <= manyHexagonals; currentNum++) {
            for (int i = -currentNum; i < currentNum + 1; i++) {
                for (int j = -currentNum; j < currentNum + 1; j++) {
                    for (int k = -currentNum; k < currentNum + 1; k++) {
                        if ((i + j == currentNum && k == -currentNum) || (i + j == -currentNum && k == currentNum)) {
                            addHexArray(new Hexagonal(i, j, k));
                        } else if ((i + k == currentNum && j == -currentNum)
                                || (i + k == -currentNum && j == currentNum)) {
                            addHexArray(new Hexagonal(i, j, k));
                        } else if ((j + k == currentNum && i == -currentNum)
                                || (j + k == -currentNum && i == currentNum)) {
                            addHexArray(new Hexagonal(i, j, k));
                        }
                    }
                }
            }
        }
        // create new player
        for (int i = 0; i < manyPlayers; i++) {
            players.add(new Player("Player " + i));
        }
    }

    /**
     * Method for adding a hexagonal into an array avoiding doubles
     * 
     * @param hex Hexagonal that needs to be add to an array
     */
    public void addHexArray(Hexagonal hex) {
        if (callHexSlot(hex.getX(), hex.getY(), hex.getZ()) == null) {
            hexArray.add(hex);
        }
    }

    /**
     * For Calling a Hexagonal object using its coordinate from a hexagonal array
     * 
     * @param x X coordinate of Hexagonal
     * @param y Y coordinate of Hexagonal
     * @param z Z coordinate of Hexagonal
     * @return a Hexagonal that matches the inputs
     */
    public Hexagonal callHexSlot(int x, int y, int z) {
        for (Hexagonal hex : hexArray) {
            if (hex.getX() == x && hex.getY() == y && hex.getZ() == z) {
                return hex;
            }
        }
        return null;
    }

    /**
     * For checking if all the hexagonals in board are filled with chips
     * 
     * @return True if all is filled and False if there's still an empty slot
     */
    public boolean checkAllIsFilled() {
        for (Hexagonal hex : hexArray) {
            if (hex.getIsFilled() == false) {
                return false;
            }
        }
        return true;
    }

    /**
     * For checking if the hexagonals between a x coordinate is filled or not. This
     * method is used for flipping the chips especially the temporary array of
     * hexagons
     * 
     * @param x       A x coordinate
     * @param y1      A y coordonate
     * @param y2      A y coordinate
     * @param z1      A z coordinate
     * @param z2      A z coordinate
     * @param current a player
     * @return True if it's all filled
     */
    public boolean checkBetweenIsFilledX(int x, int y1, int y2, int z1, int z2, Player current) {
        // Check if the temporary array has a fill or not
        if (this.hextemp.size() != 0) {
            this.hextemp.clear();
        }
        for (Hexagonal hex : hexArray) {
            if (y2 > y1 && hex.getX() == x) {
                if (z2 > z1) {
                    if (hex.getY() > y1 && hex.getZ() > z1 && hex.getY() < y2 && hex.getZ() < z2) {
                        this.hextemp.add(hex);
                    }
                } else if (z2 < z1) {
                    if (hex.getY() > y1 && hex.getZ() < z1 && hex.getY() < y2 && hex.getZ() > z2) {
                        this.hextemp.add(hex);
                    }
                }
            } else if (y1 > y2 && hex.getX() == x) {
                if (z2 > z1) {
                    if (hex.getY() < y1 && hex.getZ() > z1 && hex.getY() > y2 && hex.getZ() < z2) {
                        this.hextemp.add(hex);
                    }
                } else if (z2 < z1) {
                    if (hex.getY() < y1 && hex.getZ() < z1 && hex.getY() > y2 && hex.getZ() > z2) {
                        this.hextemp.add(hex);
                    }
                }
            }
        }
        for (Hexagonal hex : this.hextemp) {
            if (hex.getIsFilled() == false || hextemp.size() == 0 || hex.getChip().getPlayer() == current) {
                return false;
            }
        }
        return true;
    }

    /**
     * For checking if the hexagonals between a y coordinate are filled or not. This
     * method is used for flipping the chips especially the temporary array of
     * hexagons
     * 
     * @param x1      X coordinate
     * @param x2      A x coordinate
     * @param y       A y coordinate
     * @param z1      A z coordinate
     * @param z2      A z coordinate
     * @param current a player
     * @return True if it's all filled
     */
    public boolean checkBetweenIsFilledY(int x1, int x2, int y, int z1, int z2, Player current) {
        if (this.hextemp.size() != 0) {
            this.hextemp.clear();
        }
        for (Hexagonal hex : hexArray) {
            if (x2 > x1 && hex.getY() == y) {
                if (z2 > z1) {
                    if (hex.getX() > x1 && hex.getZ() > z1 && hex.getX() < x2 && hex.getZ() < z2) {
                        this.hextemp.add(hex);
                    }
                } else if (z2 < z1) {
                    if (hex.getX() > x1 && hex.getZ() < z1 && hex.getX() < x2 && hex.getZ() > z2) {
                        this.hextemp.add(hex);
                    }
                }
            } else if (x1 > x2 && hex.getY() == y) {
                if (z2 > z1) {
                    if (hex.getX() < x1 && hex.getZ() > z1 && hex.getX() > x2 && hex.getZ() < z2) {
                        this.hextemp.add(hex);
                    }
                } else if (z2 < z1) {
                    if (hex.getX() < x1 && hex.getZ() < z1 && hex.getX() > x2 && hex.getZ() > z2) {
                        this.hextemp.add(hex);
                    }
                }
            }
        }
        for (Hexagonal hex : this.hextemp) {
            if (hex.getIsFilled() == false || hextemp.size() == 0 || hex.getChip().getPlayer() == current) {
                return false;
            }
        }
        return true;
    }

    /**
     * For checking if the hexagonals between a z coordinate are filled or not. This
     * method is used for flipping the chips especially the temporary array of
     * hexagons
     * 
     * @param x1      A x coordinate
     * @param x2      A x coordinate
     * @param y1      A y coordinate
     * @param y2      A z coordinate
     * @param z       A z coordinate
     * @param current a player
     * @return True if it's all filled
     */
    public boolean checkBetweenIsFilledZ(int x1, int x2, int y1, int y2, int z, Player current) {
        if (this.hextemp.size() != 0) {
            this.hextemp.clear();
        }
        for (Hexagonal hex : hexArray) {
            if (y2 > y1 && hex.getZ() == z) {
                if (x2 > x1) {
                    if (hex.getY() > y1 && hex.getX() > x1 && hex.getY() < y2 && hex.getX() < x2) {
                        this.hextemp.add(hex);
                    }
                } else if (x2 < x1) {
                    if (hex.getY() > y1 && hex.getX() < x1 && hex.getY() < y2 && hex.getX() > x2) {
                        this.hextemp.add(hex);
                    }
                }
            } else if (y1 > y2 && hex.getZ() == z) {
                if (x2 > x1) {
                    if (hex.getY() < y1 && hex.getX() > x1 && hex.getY() > y2 && hex.getX() < x2) {
                        this.hextemp.add(hex);
                    }
                } else if (x2 < x1) {
                    if (hex.getY() < y1 && hex.getX() < x1 && hex.getY() > y2 && hex.getX() > x2) {
                        this.hextemp.add(hex);
                    }
                }
            }
        }
        for (Hexagonal hex : this.hextemp) {
            if (hex.getIsFilled() == false || hextemp.size() == 0 || hex.getChip().getPlayer() == current) {
                return false;
            }
        }
        return true;
    }

    /**
     * Simplify method for removing chip and input it to a temporary array
     * 
     * @param hex     A Hexagonal slot where the chip needs to change its current
     *                possession
     * @param current Current player
     */
    public void chipSimple(Hexagonal hex, Player current) {
        Chip chip = hex.getChip();
        if (chip.getPlayer() != current) {
            chip.getPlayer().getChips().remove(chip);
            chipTemps.add(chip);
        }
    }

    /**
     * Method for flipping other player's chips
     * 
     * @param playerNow A player that wants to flip other players or current
     *                  player's turn
     */
    public void chipsFlipper(Player playerNow) {
        // This can be done if there's at least 2 chips in possession
        if (playerNow.getChips().size() >= 2) {
            Chip latestChip = playerNow.getChips().get(playerNow.getChips().size() - 1);
            int latestX = latestChip.getHexSlot().getX();
            int latestY = latestChip.getHexSlot().getY();
            int latestZ = latestChip.getHexSlot().getZ();

            // Check all chips in the current player
            for (Chip chipNow : playerNow.getChips()) {
                int x = chipNow.getHexSlot().getX();
                int y = chipNow.getHexSlot().getY();
                int z = chipNow.getHexSlot().getZ();

                // If the chip is same as the latest, skip it
                if (chipNow == latestChip) {
                    continue;
                }

                // Check and flip all the chips between x coordinate
                // Note : if there's AN EMPTY HEXAGONAL between thecoordinates then all the
                // chips between can't be flipped
                if (x == latestX) {
                    if (checkBetweenIsFilledX(x, y, latestY, z, latestZ, playerNow) == true) {
                        for (Hexagonal hex : this.hextemp) {
                            chipSimple(hex, playerNow);
                        }

                    }
                }

                // Check and flip all the chips between y coordinate
                // Note : if there's AN EMPTY HEXAGONAL between the coordinates then all the
                // chips between can't be flipped
                if (y == latestY) {
                    if (checkBetweenIsFilledY(x, latestX, y, z, latestZ, playerNow) == true) {
                        for (Hexagonal hex : this.hextemp) {
                            chipSimple(hex, playerNow);
                        }
                    }
                }

                // Check and flip all the chips between z coordinate
                // Note : if there's AN EMPTY HEXAGONAL between the coordinates then all the
                // chips between can't be flipped
                if (z == latestZ) {
                    if (checkBetweenIsFilledZ(x, latestX, y, latestY, z, playerNow) == true) {
                        for (Hexagonal hex : this.hextemp) {
                            chipSimple(hex, playerNow);
                        }
                    }
                }
            }

            // Adding all the temporary chips into current player's array of chips
            for (Chip ch : chipTemps) {
                playerNow.addChipToArrayCon(ch);
            }
            // clear the temporary array for next uses
            chipTemps.clear();
        }
    }

    /**
     * Method for sorting who has most chips in hand to the least
     * 
     * @return a new array of players
     */
    public ArrayList<Player> checkWinner() {
        ArrayList<Player> winnerLooser = new ArrayList<>();
        Player thisPlayer = players.get(0);
        winnerLooser.add(thisPlayer);
        for (Player otherPlayer : players) {
            if (thisPlayer.getTotalChips() > otherPlayer.getTotalChips() && thisPlayer != otherPlayer) {
                // If the other player not in arraylist then add to some index
                if (winnerLooser.indexOf(otherPlayer) == -1) {
                    for (Player anotherPlayer : winnerLooser) {
                        if (anotherPlayer.getTotalChips() < otherPlayer.getTotalChips()) {
                            winnerLooser.add(winnerLooser.indexOf(anotherPlayer), otherPlayer);
                        }
                    }
                    if (winnerLooser.indexOf(otherPlayer) == -1) {
                        winnerLooser.add(winnerLooser.size(), otherPlayer);
                    }
                }
            }

            else if (thisPlayer.getTotalChips() <= otherPlayer.getTotalChips() && thisPlayer != otherPlayer) {
                // If the other player not in arraylist then add to some index
                if (winnerLooser.indexOf(otherPlayer) == -1) {
                    winnerLooser.add(0, otherPlayer);
                }
                thisPlayer = otherPlayer;
            } else
                continue;
        }
        return winnerLooser;
    }

    /**
     * Getter for array of players
     * 
     * @return an array of players
     */
    public ArrayList<Player> getPlayers() {
        return this.players;
    }

    /**
     * Getter of array of hexagonals
     * 
     * @return an array of hexagonals
     */
    public ArrayList<Hexagonal> getHexArray() {
        return this.hexArray;
    }
}